This is an attempt at simulating collision prediction in a closed n-body system with constant energy.

It is written in Python, but mostly relies on Numpy and Matplotlib for convenience.

Instead of doing `N(N - 1)/2` checks every frame, the system uses linear prediction to only perform updates on the initial setup and after each collision.