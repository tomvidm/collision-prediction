# N-Body System
This class simulates a system of N circular bodies defined by the following state variables:

* Their positions
* Their velocities
* Their radii
* Their masses
* The current time of the system

## Collision detection
At initialization, the system checks each body against all other bodies to find the earliest collision. This collision will be stored in a structure `Collision` in a list at their respective indexes.

### Update step
During update, the system will loop through all collision predictions. If the time until the collision is less than the delta T passed to the update method, the collision will be resolved.

A vector `collision_mask` will also have the entries corresponding to bodies involved in the collisions, set to 0. This vector will be multiplied into the velocity vector to avoid updating the positions a second time after the collision resolution.

### Orphaned collisions
Consider the simple system with 3 bodies below. During the initialization, the system will first check 0 against 1. A collision is found between 0 and 1. The calculated time until impact is `dt_01`. Next, the system will check 0 against 2, and a new collision is detected. The calculated time is `dt_02` and it is found that `dt_02 < dt_01`. T

At the current state, there is a corresponding pair for body 0 and 2, and only body 1 believes that there will be a collision with body 0 after `dt_01` time. This collision prediction is *orphaned* and must be ignored during the update step.
```
After checking 0 against 1:
0 - (0 -x 1, dt_01)
1 - (0 -x 1, dt_01)
2 - ?

After checking 0 against 2:
0 - (0 -x 2, dt_02)
1 - (0 -x 1, dt_01)
2 - (0 -x 2, dt_02)
```

There are three cases:
* There exists a pair `(n, k)` such that the corresponding collision predictions `P(n)` and `P(k)` are equal.
* There is a prediction `p` in `P` which is unique.
* There is zero or more `p` in `P` which signals non-collision.

