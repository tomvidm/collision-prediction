import numpy as np
from numpy import array, dot, zeros, ones
from numpy.linalg import norm
from enum import Enum


import matplotlib.pyplot as plt
from matplotlib import animation

inf = 99999999999.

class Collision:
    def __init__(self, a, b, collides, time_of_impact):
        self.a = a
        self.b = b
        self.collides = collides
        self.toi = time_of_impact

class NBodySystem:
    def __init__(self, N):
        self.num = N
        self.pos = zeros((N, 2))
        self.vel = zeros((N, 2))
        self.mass = ones(self.num)
        self.radii = ones(self.num)
        
        self.time = 0.
    
        self.predicted_collisions = [Collision(-1, -1, False, inf)]*self.num
    # ========
    # UPDATING
    # ========
    
    def step(self, n, dt):
        self.pos[n] += self.vel[n] * dt
    
    def step_all(self, dt, mask=None):
        if mask is None:
            self.pos += self.vel * dt
        else:
            self.pos += np.multiply(self.vel.T, mask).T * dt
    
    def update(self, dt):
        self.print_collisions()
        collision_mask = np.full(self.num, 1.)
        for n in range(self.num):
            pred = self.predicted_collisions[n]
            if pred.collides is True:
                if self.predicted_collisions[pred.a] == self.predicted_collisions[pred.b]:
                    if pred.toi - self.time < dt:
                        print("Collision!", pred.a, "-x-", pred.b, " with |p_a - p_b| =", norm(self.pos[pred.a] - self.pos[pred.b]))
                        self.resolve_collision(pred, dt)
                        collision_mask[pred.a] = 0.
                        collision_mask[pred.b] = 0.
                        
                        self.foo(pred)
                else:
                    print(n, "is orphaned.")
                    new_pred = self.find_earliest_collision(n)
                    if new_pred.collides is True:
                        print("\t", new_pred.a, "-x", new_pred.b)
                        self.predicted_collisions[new_pred.a] = new_pred
                        self.predicted_collisions[new_pred.b] = new_pred
                    else:
                        print("\t", n, "-x")
                        self.predicted_collisions[n] = new_pred
                        
        self.step_all(dt, collision_mask)
        self.time += dt

    def foo(self, old_pred):
        # For old_pred.a
        new_pred = self.find_earliest_collision(old_pred.a, old_pred.b)
        if new_pred.collides is True:
            print("\t", new_pred.a, "-x", new_pred.b)
            self.predicted_collisions[new_pred.a] = new_pred
            self.predicted_collisions[new_pred.b] = new_pred
        else:
            print("\t", old_pred.a, "-x")
            self.predicted_collisions[old_pred.a] = new_pred
            
        # For old_pred.b
        new_pred = self.find_earliest_collision(old_pred.b, old_pred.a)
        if new_pred.collides is True:
            print("\t", new_pred.a, "-x", new_pred.b)
            self.predicted_collisions[new_pred.a] = new_pred
            self.predicted_collisions[new_pred.b] = new_pred
        else:
            print("\t", old_pred.b, "-x")
            self.predicted_collisions[old_pred.b]
        
    
    def prediction_is_valid(self, pred):
        if pred.collides is not True:
            return True
        else:
            if self.predicted_collisions[pred.a] == self.predicted_collisions[pred.b]:
                return True
            else:
                return False
        
    def prediction_is_orphaned(self, pred):
        if self.predicted_collisions[pred.a] != self.predicted_collisions[pred.b]:
            return True
        else:
            return False
        
    # ===================
    # GETTERS AND SETTERS
    # ===================

    def set_pos(self, pos):
        self.pos = pos
        
    def set_vel(self, vel):
        self.vel = vel        

    def set_pos_of(self, n, pos):
        self.pos[n] = pos
        
    def get_pos(self):
        return self.pos
    
    def get_vel(self):
        return self.vel
        
    def get_pos_of(self, n):
        return self.pos[n]
    
    def get_vel_of(self, n):
        return self.vel[n]
    
    # =========
    # COLLISION
    # =========
    
    def predict_circular_collision(self, a, b):
        r_sum = self.radii[a] + self.radii[b]
        diff_p = self.pos[b] - self.pos[a]
        diff_v = self.vel[b] - self.vel[a]
        
        if diff_v[0] == 0. and diff_v[1] == 0.:
            return Collision(-1, -1, False, inf)
        
        dot_pp = dot(diff_p, diff_p)
        dot_pv = dot(diff_p, diff_v)
        dot_vv = dot(diff_v, diff_v)
    
        determinant = (dot_pv**2)/(dot_vv**2) - (dot_pp - r_sum**2)/dot_vv
        
        if determinant < 0.:
            return Collision(a, b, False, inf)
        
        dt0 = -dot_pv / dot_vv - np.sqrt(determinant)
        dt1 = dt0 + 2*np.sqrt(determinant)
        if dt1 < 0.: # t1 is always larger than t0
            return Collision(a, b, False, inf)
        elif dt0 < 0.:
            return Collision(a, b, True, self.time + dt0)
        else:
            return Collision(a, b, True, self.time + dt0)
        
    
    def find_earliest_collision(self, n, excluded=None):
        # Assume no collision
        earliest_prediction = Collision(-1, -1, False, inf)
        
        # Check against all other particles
        for k in range(self.num):
            if k == n or k == excluded:
                continue
            prediction = self.predict_circular_collision(n, k)
            if (prediction.toi < earliest_prediction.toi) and (prediction.toi < self.predicted_collisions[n].toi) and (prediction.toi < self.predicted_collisions[k].toi):
                earliest_prediction = prediction
               
        return earliest_prediction
            
    def resolve_collision(self, coll, dt):
        dt_until_contact = coll.toi - self.time
        dt_after_contact = dt - dt_until_contact

        a = coll.a
        b = coll.b
        
        self.step(a, dt_until_contact)
        self.step(b, dt_until_contact)

        diff_p = self.pos[b] - self.pos[a]
        diff_v = self.vel[b] - self.vel[a]
        m_a = self.mass[a]
        m_b = self.mass[b]      
        mr_a = 2 * m_b / (m_a + m_b)
        mr_b = 2 * m_a / (m_a + m_b)      
        vr = dot(diff_p, diff_v) / dot(diff_p, diff_p)      
        dv_a = mr_a * vr * diff_p
        dv_b = -mr_b * vr * diff_p
        
        self.vel[a] += dv_a
        self.vel[b] += dv_b
        
        self.step(a, dt_after_contact)
        self.step(b, dt_after_contact)
        
        
        
    
    def setup_initial_predictions(self):
        for n in range(self.num):
            prediction = self.find_earliest_collision(n)
            if prediction.collides == True:
                self.predicted_collisions[prediction.a] = prediction
                self.predicted_collisions[prediction.b] = prediction
    
    # ======================
    # RANDOM DEBUGGING STUFF
    # ======================
    def print_collisions(self):
        for n in range(self.num):
            coll = self.predicted_collisions[n]
            if coll.collides is True:
                print(n, "-", coll.a, "-x", coll.b, "\t", coll.toi)
            else:
                print(n, "-", n, "-x")
            
    def total_kinetic_energy(self):
        p_tot = 0.
        for n in range(self.num):
            p_tot += 0.5 * self.mass[n] * dot(self.vel[n], self.vel[n])
        return p_tot
    
    def perturb(self):
        self.pos = np.random.uniform(-5., 5., self.pos.shape)
        self.vel = np.random.normal(0., 0.1, self.vel.shape)

system = NBodySystem(4)
system.set_pos(array([[-4., 0.], [0., 0.5], [4., 1.], [-1., -1.5]]))
system.set_vel(array([[0.5, 0.], [0., 0.], [0., 0.], [0., 0.]]))
#system.perturb()
system.setup_initial_predictions()

fig, ax = plt.subplots(figsize=(6, 6))
ax.set(xlim=(-6, 6), ylim=(-6, 6))

initial_pos = system.get_pos()

scat = ax.scatter(initial_pos[:, 0], initial_pos[:, 1])
dt = 0.4
def animate(i):
    print("Frame:", i, "\tTotal energy:", system.total_kinetic_energy(), "\Time:", system.time)
    system.update(dt)
    scat.set_offsets(system.get_pos())

    return scat
    
anim = animation.FuncAnimation(fig, animate, interval=100)

plt.show()